﻿using Google.Contacts;
using Google.GData.Client;
using Google.GData.Contacts;

namespace ContactImageSyncer
{
    public class ContactsBLL
    {
        private FileReader _fileReader;

        public ContactsBLL()
        {
            _fileReader = new FileReader();
        }

        public ContactsRequest BuildContactsRequest()
        {
            var oAuthSettings = _fileReader.GetOAuthSettings();

            var requestSettings = new RequestSettings(oAuthSettings.ApplicationName)
            {
                OAuth2Parameters = new OAuth2Parameters
                {
                    ClientId = oAuthSettings.ClientId,
                    ClientSecret = oAuthSettings.ClientSecret,
                    RefreshToken = oAuthSettings.RefreshToken,
                    AccessToken = oAuthSettings.AccessToken
                }
            };

            return new ContactsRequest(requestSettings);
        }

        public ContactsQuery BuildContactsQuery()
        {
            return new ContactsQuery(ContactsQuery.CreateContactsUri("default"))
            {
                NumberToRetrieve = 500
            };
        }
    }
}
