﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ContactImageSyncer
{
    public class FileReader
    {
        private static IConfiguration _config;

        public FileReader()
        {
            _config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();
        }

        public OAuthSettings GetOAuthSettings()
        {
            var jsonFilePath = _config["OAuthFilePath"];
            
            return JsonConvert.DeserializeObject<OAuthSettings>(File.ReadAllText(jsonFilePath));
        }

        public static string GetLoggingFilePath()
        {
            return _config["LoggingFilePath"];
        }
    }
}
