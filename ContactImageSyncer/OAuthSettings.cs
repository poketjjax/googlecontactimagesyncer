﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactImageSyncer
{
    public class OAuthSettings
    {
        public string ApplicationName { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
    }
}
