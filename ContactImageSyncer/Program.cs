﻿using System;
using System.IO;
using System.Net.Http;
using Google.Contacts;

namespace ContactImageSyncer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var bll = new ContactsBLL();

                var contactsRequest = bll.BuildContactsRequest();
                var query = bll.BuildContactsQuery();
                var allContacts = contactsRequest.Get<Contact>(query);
                var client = new HttpClient();

                foreach (var contact in allContacts.Entries)
                {
                    if (!string.IsNullOrWhiteSpace(contact.Content))
                    {
                        try
                        {
                            var stream = client
                                .GetStreamAsync($"http://graph.facebook.com/v3.3/{contact.Content}/picture?type=large")
                                .Result;

                            contactsRequest.SetPhoto(contact, stream);
                        }
                        catch (Exception ex)
                        {
                            var message = $"{DateTime.Now}: Error updating photo for {contact.Name?.FullName}. Content = {contact.Content}. Exception message = {ex.Message}";
                            File.AppendAllLines(FileReader.GetLoggingFilePath(), new[] {message});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllLines(FileReader.GetLoggingFilePath(), new[] {$"{DateTime.Now}: {ex}"});
            }
        }
    }
}
